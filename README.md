Driver navigation application for https://bitbucket.org/Velizar_/ironing-business-management/overview

Downloads the route data and navigates between customers with a single button click per customer. 

Also tracks GPS coordinates in real time which can be found on the map at Trips.

# Compilation

Standard compilation as described at https://developer.android.com/studio/run/index.html

# Architecture

Look in the source code, most individual non-trivial classes have a comment indicating their role in the program.

## Mapping

What to find where.

- Anything happening on the main page is referenced from MainActivity
- Global mutable state is stored in State
- Trip and TripItem are just used as containers to pass around
- Obtaining the daily trip is started from MainActivity, downloaded via ServerBackgroundConnector, parsed via Trip and TripItem, and stored in State
- Displaying the trip is done via TripItemsAdapter, called from MainActivity which passes the data
- Location tracking is started from MainActivity, calling LocationTracker, and sending data via ServerBackgroundConnector
- Double-clicking on a trip item brings up TripItemDialog and the result is stored in that TripItem object
- The Settings dialogs are displayed(!) and handled by State
- The notification which shows up on starting a trip is created and managed entirely by MainActivity; clicking Next sends a NextCustomerService

# Development

Trip has a mockTrip option, which can be used instead of retrieving a trip from a server on clicking Start.

# Screenshots

![alt tag](http://i.imgur.com/nf8jFZv.png)

Settings (Upload is not implemented)

![alt tag](http://i.imgur.com/fcAetuk.png)

Trip list - clicking Start launches Waze at that item

![alt tag](http://i.imgur.com/98Q7hQf.png)

The notification, as seen on Android 4.  
Clicking the main area opens the application;  
Clicking Next opens Waze at the next customer, with name 'Customer 32'.

![alt tag](http://i.imgur.com/4hiDCkZ.png)

Double-clicking an item allows input of data.  
NOTE: Money owed system not implemented yet.

![alt tag](http://i.imgur.com/loDJmOq.png)

The list after some data is entered.
