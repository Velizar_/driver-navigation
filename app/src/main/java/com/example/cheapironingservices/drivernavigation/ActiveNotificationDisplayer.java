package com.example.cheapironingservices.drivernavigation;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class ActiveNotificationDisplayer extends Service {
    public final static int NOTIFICATION_ID = 0;
    public final static String CHANNEL_ID = "1";
    private NotificationManager notificationManager = null;

    // text used for the notification
    private String currentCustomerInformation(TripItem ti) {
        String str = ti.customerName + " - " + ti.timeFormat.format(ti.visitAt) + ", ";
        if (ti.collectionOrDelivery == TripItem.COLLECTION_OR_DELIVERY.COLLECTION)
        {
            str += "collection";
        }
        else // on delivery
        {
            str += "delivery";
            // inform about bags and hangers
            boolean hasBags = (ti.numberOfBags > 0);
            String bagsString = ti.numberOfBags + " bag(s)";
            if (ti.hasHangers && hasBags)
                str += " (" + bagsString + " + hangers)";
            else if (ti.hasHangers && !hasBags)
                str += " (only hangers)";
            else if (!ti.hasHangers && hasBags)
                str += " (" + bagsString + ")";
            // if (!hasHangers && !hasBags) append nothing
            str += String.format(", %s", Functions.asCurrency(ti.moneyOwed, true));
        }
        if (!ti.notes.isEmpty())
            str += ", " + ti.notes;
        return str;
    }

    private void createNotificationChannelIfApplicable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String name = "Convenience";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /* Shows the main activity bar allowing quick actions in the application
     *
     * Clicking it restores the application;
     * The Next button informs the application that the driver has reached the customer,
     *   updates the application state to reflect that,
     *   and sends Waze to the next customer's geocode.
     */
    private void showActivityBar(TripItem item) {
        PendingIntent restorePendingIntent = PendingIntent.getActivity(this, 1,
                Functions.bringOnTop(this, MainActivity.class), 0);

        Intent nextIntent = new Intent(this, NextCustomerService.class);
        PendingIntent nextPendingIntent = PendingIntent.getService(this, 1224, nextIntent, PendingIntent.FLAG_ONE_SHOT);
        Intent nextWithoutMarkIntent = new Intent(this, NextCustomerService.class);
        nextWithoutMarkIntent.putExtra("customer paid", false);
        PendingIntent nextWithoutMarkPendingIntent = PendingIntent.getService(this, 1225, nextWithoutMarkIntent, PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentIntent(restorePendingIntent)
                .setSmallIcon(R.drawable.iron_notification)
                .setContentTitle("to " + item.fullAddress)
                .setContentText("Slide down to reveal actions...")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(
                        currentCustomerInformation(item) ))
                .setOngoing(true)
                .addAction(R.drawable.next, "Next: " + State.trip.nextLocationName(), nextPendingIntent)
                .addAction(R.drawable.next, "Next (did not pay)", nextWithoutMarkPendingIntent)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(0)
                .build();

        startForeground(948, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannelIfApplicable();
        showActivityBar((TripItem) intent.getSerializableExtra("trip item"));
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        notificationManager.cancel(NOTIFICATION_ID);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
