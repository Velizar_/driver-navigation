package com.example.cheapironingservices.drivernavigation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Trip {
    public TripItem[] tripItems;
    public int currentTripItemIdx;
    public final int id;

    public Trip(JSONObject params) throws JSONException {
        JSONArray tripItemsJSON = params.getJSONArray("tripItems");
        tripItems = new TripItem[tripItemsJSON.length()];
        for (int i = 0; i < tripItemsJSON.length(); i++)
            tripItems[i] = new TripItem(tripItemsJSON.getJSONObject(i));
        currentTripItemIdx = 0;
        id = params.getInt("id");
    }

    public static Trip mockTrip() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", 1);

            String[] geocodesArray = {"51.507268,-0.16573", "51.874417,-0.374161",
                    "51.4158128,0.0240241", "51.4552507,-0.0668139", "51.4406958,-0.0453841",
                    "51.4187926,-0.0326697", "51.4869717,0.09725"};
            String[] visitAtArray = {"18:33", "18:44", "18:59", "19:15", "19:32", "19:46", "20:04"};
            String[] namesArray = {"John", "Marie", "Jack", "Fred", "Lilly", "Ivan", "Kent"};
            String[] addressesArray = {"105 Alscot Road, SE1", "172 Alnwick Road, SE12 9BT",
                    "2 Marigold Street, SE16 4PF", "120-130 Breakspears Road, SE4",
                    "147B Brook Drive, SE11 4TQ", "23 Pellipar Road, SE18 5EG",
                    "80 Basildon Road, SE2 0EP"};
            int[] idArray = {1, 2, 3, 4, 5, 6, 7};
            float[] moneyArray = {0.0f, 15.00f, 12.25f, 0.0f, 9.30f, 30.15f, 0.0f};
            int[] bagsArray = {0, 2, 1, 0, 0, 1, 0};
            boolean[] hangersArray = {false, false, true, false, true, true, false};

            JSONArray tripItemsArray = new JSONArray();
            for (int j = 0; j < 3; j++) // three copies of each trip item
                for (int i = 0; i < 7; i++) {
                    JSONObject innerObj = new JSONObject();
                    innerObj.put("geocode", geocodesArray[i])
                            .put("visitAt", visitAtArray[i])
                            .put("customerName", namesArray[i])
                            .put("fullAddress", addressesArray[i])
                            .put("moneyOwned", moneyArray[i])
                            .put("numberOfBags", bagsArray[i])
                            .put("hasHangers", hangersArray[i])
                            .put("id", idArray[i]);
                    tripItemsArray.put(innerObj);
                }
            obj.put("tripItems", tripItemsArray);
            return new Trip(obj);
        }
        // will never happen because this JSON is hardcoded
        catch (JSONException e)
        {
            return null;
        }
    }

    public TripItem currentTripItem() { return tripItems[currentTripItemIdx]; }

    public String nextLocationName() {
        if (currentTripItemIdx == tripItems.length - 1)
            return "Home";
        else
            return tripItems[currentTripItemIdx + 1].customerName;
    }
}
