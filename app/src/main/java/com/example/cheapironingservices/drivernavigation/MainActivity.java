package com.example.cheapironingservices.drivernavigation;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {

    private static MainActivity instance;
    private static LocationTracker tracker;
    private static TripItemsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;
        setContentView(R.layout.activity_main);

        // Initialize list
        ListView tripItemsList = findViewById(R.id.tripItemsList);
        adapter = TripItemsAdapter.newInstance(this, android.R.layout.simple_list_item_1);
        tripItemsList.setAdapter(adapter);
        tripItemsList.setClickable(true);
        tripItemsList.setOnItemClickListener(TripItemsAdapter.getListener());

        // if trip data is already downloaded
        if (State.trip != null)
            tripActiveState();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        State.loadOrPromptServerData(this);
        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFA0A0A0")));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Prompt for username and password
        // pre-fill with any existing values
        if (item.getItemId() == R.id.action_account) {
            State.promptLoginCredentials(this);
            return true;
        }
        // Popup a text field for the new URL
        // pre-fill field with existing URL
        else if (item.getItemId() == R.id.action_change_url) {
            State.promptWebsiteURL(this);
            return true;
        }
        // Call onNext for each trip item
        else if (item.getItemId() == R.id.action_mark_all) {
            setRemainingCompleted(State.trip);
        }
        // Upload data to the server
        else if (item.getItemId() == R.id.action_upload) {
            upload(State.trip);
        }
        else if (item.getItemId() == R.id.action_restart) {
            restart();
        }
        else if (item.getItemId() == R.id.action_exit) {
            restart(); // reset interface and hide the notification
            this.finishAffinity();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == State.PERMISSION_TO_TRACK &&
                grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            tracker.tryToStartTracking(this);
        }
    }

    public void startTrip(View v) {
        boolean requiredDataPresent = false;

        // prompt for up to one missing piece of login information per Start click
        if (State.websiteURL.isEmpty())
            State.promptWebsiteURL(this);
        else if (State.username.isEmpty() || State.password.isEmpty())
            State.promptLoginCredentials(this);
        else
            requiredDataPresent = true;

        if (!requiredDataPresent)
            return;

        boolean tripRetrieved = false;
        // Get trip
        try
        {
//            Trip t = Trip.mockTrip(); // for testing
            Trip t = new Trip(TripInfoFetcher.blockingRequestTrip());
            if (t.tripItems.length == 0)
                throw new EmptyTripException();

            State.trip = t;

            // transform GUI
            tripActiveState();

            tripRetrieved = true;
        }
        catch (JSONException e)
        {
            Toast.makeText(getApplicationContext(), "Cannot load trip, read unexpected data. Error: "
                    + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        catch (UnknownHostException e)
        {
            Toast.makeText(getApplicationContext(), "Cannot resolve server, are you using the correct URL? Details: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
        catch (IOException e)
        {
            Toast.makeText(getApplicationContext(), "Server timeout, please try again. Full message: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
        catch (InvalidCredentialsException e)
        {
            Toast.makeText(getApplicationContext(), "Invalid login credentials",
                    Toast.LENGTH_LONG).show();
            State.promptLoginCredentials(this);
        }
        catch (NoTripAssignedException e)
        {
            Toast.makeText(getApplicationContext(), "No trip assigned to your account, please inform administrator.",
                    Toast.LENGTH_LONG).show();
        }
        catch (EmptyTripException e)
        {
            Toast.makeText(getApplicationContext(), "Empty trip for the day, please inform administrator.",
                    Toast.LENGTH_LONG).show();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "Unknown error: " + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        // Turn on tracking
        if (tripRetrieved)
        {
            tracker = new LocationTracker(getApplicationContext());
            tracker.requestPermissionToTrackIfMissing(this);
            tracker.tryToStartTracking(this);
        }

        /*
        // save to state
        try {
            State.trip = Trip.mockTrip();

            // transform GUI
            tripActiveState();
        } catch(JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        */
    }

    public void goToSelectedTripItem(View v) {
        try
        {
            TripItem item = State.trip.currentTripItem();
            Functions.sendToWaze(this, item);
            showActivityBar(item);
        }
        catch (ActivityNotFoundException ex)
        {
            Toast.makeText(this, "Error: Waze not installed.", Toast.LENGTH_SHORT).show();
        }
    }

    // Minimize on back button
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
        {
            this.moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void tripActiveState() {
        findViewById(R.id.goBtn).setVisibility(View.VISIBLE);
        findViewById(R.id.startBtn).setVisibility(View.INVISIBLE);
        adapter.setTrip(State.trip);
    }

    public void showActivityBar(TripItem item) {
        LocationTracker.requestPermissionToTrackIfMissing(this);
        Intent intent = new Intent(this, ActiveNotificationDisplayer.class);
        intent.putExtra("trip item", item);
        startService(intent);
    }

    public void hideActivityBar() {
        Intent intent = new Intent(this, ActiveNotificationDisplayer.class);
        stopService(intent);
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public static void refreshList() {
        adapter.notifyDataSetChanged();
    }

    private void setRemainingCompleted(final Trip trip) {
        Runnable onYes = () -> {
            for (TripItem item : trip.tripItems)
                item.setState(item.moneyOwed, TripItem.STATE.COMPLETED);
            refreshList();
        };

        Functions.withConfirmation(
                MainActivity.this,
                "Are you sure you wish to mark all white trip items as completed and fully paid?",
                onYes);
    }

    private void upload(Trip trip) {
        try {
            JSONObject obj = new JSONObject();
            JSONArray tripItems = new JSONArray();
            for (TripItem ti : trip.tripItems) {
                JSONObject tripItem = new JSONObject();
                tripItem.put("id", ti.id);
                int paidInt = ti.paid.multiply(new BigDecimal(100)).intValue();
                tripItem.put("paid", paidInt);
                String state = ti.state.toString().toLowerCase();
                if (state.equals("not_completed"))
                    state = "unmarked";
                tripItem.put("state", state);
                tripItems.put(tripItem);
            }
            obj.put("tripItems", tripItems);
            obj.put("username", State.username);
            obj.put("password", State.password);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
            RequestBody body = RequestBody.create(obj.toString(), MediaType.parse("application/json; charset=utf-8"));
            Request request = new Request.Builder()
                    .url(State.websiteURL + "/driver_location/upload_trip_status")
                    .post(body)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                @Override public void onResponse(@NotNull Call call, @NotNull Response response) { }
            });
            Toast.makeText(getApplicationContext(), "Initiated upload.", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void restart() {
        // 1. transform Go to Start
        findViewById(R.id.goBtn).setVisibility(View.INVISIBLE);
        findViewById(R.id.startBtn).setVisibility(View.VISIBLE);

        // 2. make list empty
        adapter.clear();

        // 3. hide notification
        hideActivityBar();

        // 4. stop tracking location
        tracker.stop(this);

        // 5. reset trip data
        State.trip = null;
    }
}
