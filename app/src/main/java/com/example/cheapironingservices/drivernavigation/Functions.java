package com.example.cheapironingservices.drivernavigation;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/** Global functions for reuse throughout the application
 *
 */
public class Functions {
    private static Functions ourInstance = new Functions();

    public static Functions getInstance() {
        return ourInstance;
    }

    private Functions() {
    }

    public static void sendToWaze(Context ctx, TripItem ti) {
        String url = "waze://?ll=" + ti.lat + "," + ti.lng + "&navigate=yes";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(intent);
    }

    public static Intent bringOnTop(Context c, Class k) {
        return new Intent(c, k)
                .setAction(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_LAUNCHER)
                .setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    }

    /**
     *   input: 5.3, false
     *   output: "5.30"
     *
     *   input: 5, true
     *   output: "£5.00"
     */
    public static String asCurrency(BigDecimal num, boolean curSymbol) {
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(2);
        String prefix = curSymbol ? "£" : "";
        return prefix + df.format(num);
    }

    public static void withConfirmation(Context context, String promptText, final Runnable onYes) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE)
                onYes.run();
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(promptText).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
