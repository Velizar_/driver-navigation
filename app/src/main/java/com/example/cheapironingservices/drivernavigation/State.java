package com.example.cheapironingservices.drivernavigation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.InputType;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;

/** Application-wide state and state-changing functions
 *
 * Holds global state
 * Also functions which change multiple parts of the program
 */
public class State {
    private static State ourInstance = new State();

    // global variables //
    public static Trip trip = null;
    public static SharedPreferences preferences = null;
    public static String websiteURL = "";
    // driver credentials for the website
    public static String username = "";
    public static String password = "";

    // constants //
    public final static String PREFS_NAME = "Ironing app preferences";
    public final static String USERNAME_PREF = "Username";
    public final static String PASSWORD_PREF = "Password";
    public final static String WEBSITE_URL_PREF = "URL";
    public final static int PERMISSION_TO_TRACK = 1;

    private State() {
    }

    public static State getInstance() {
        return ourInstance;
    }

    public static void currentCustomerReached(boolean customerPaid) {
        // mark customer as completed and paid = owed
        TripItem item = trip.currentTripItem();
        if (customerPaid)
            item.setState(item.moneyOwed, TripItem.STATE.COMPLETED);
        else
            item.setState(new BigDecimal(0), TripItem.STATE.COMPLETED);
        // move to next customer if we're not at the last one
        if (trip.currentTripItemIdx + 1 != trip.tripItems.length) {
            trip.currentTripItemIdx++;
            MainActivity.getInstance().showActivityBar(trip.currentTripItem());
        } else {
            MainActivity.getInstance().hideActivityBar();
        }
        MainActivity.refreshList();
    }

    // Loads required data
    // If some data is missing, prompt the user to enter it
    public static void loadOrPromptServerData(Context ctx) {
        preferences = ctx.getSharedPreferences(PREFS_NAME, 0);
        websiteURL = preferences.getString(WEBSITE_URL_PREF, "");
        username = preferences.getString(USERNAME_PREF, "");
        password = preferences.getString(PASSWORD_PREF, "");

        // if data is missing, prompt the user to input it
        // if they clicked Cancel or entered empty text,
        //   the Start button will prompt them again
        // (at most one prompt will be displayed currently)
        if (websiteURL.isEmpty())
            promptWebsiteURL(ctx);
        else if (username.isEmpty() || password.isEmpty())
            promptLoginCredentials(ctx);
    }

    public static void promptWebsiteURL(Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Edit web app root URL");

        final EditText input = new EditText(ctx);
        String filledText = websiteURL.isEmpty() ? "https://" : websiteURL;
        input.setText(filledText);

        builder.setView(input);
        builder.setPositiveButton("Update", (dialog, which) -> {
            String newURL = input.getText().toString();
            if (newURL.endsWith("/"))
                newURL = newURL.substring(0, newURL.length() - 1);
            State.websiteURL = newURL;

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(State.WEBSITE_URL_PREF, newURL);
            editor.apply();
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    public static void promptLoginCredentials(Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Edit login details");

        final LinearLayout layout = new LinearLayout(ctx);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView usernameLabel = new TextView(ctx);
        usernameLabel.setText("Username");
        layout.addView(usernameLabel);

        final EditText usernameInput = new EditText(ctx);
        usernameInput.setText(State.username);
        layout.addView(usernameInput);

        final TextView passwordLabel = new TextView(ctx);
        passwordLabel.setText("Password*");
        layout.addView(passwordLabel);

        final EditText passwordInput = new EditText(ctx);
        passwordInput.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordInput.setText(State.password);
        layout.addView(passwordInput);

        final TextView passwordWarningLabel = new TextView(ctx);
        passwordWarningLabel.setText("\n\n* The password is remembered. " +
                "If the device gets stolen, change your password immediately.");
        layout.addView(passwordWarningLabel);

        builder.setView(layout);
        builder.setPositiveButton("Update", (dialog, which) -> {
            String newUsername = usernameInput.getText().toString();
            String newPassword = passwordInput.getText().toString();
            State.username = newUsername;
            State.password = newPassword;

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(State.USERNAME_PREF, newUsername);
            editor.putString(State.PASSWORD_PREF, newPassword); // store in plain text
            editor.apply();
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }
}
