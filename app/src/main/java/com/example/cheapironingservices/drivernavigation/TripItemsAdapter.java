package com.example.cheapironingservices.drivernavigation;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class TripItemsAdapter extends ArrayAdapter<TripItem> {
    // needs to be passed around
    private List<TripItem> tripItems;

    public final static int DEFAULT_ITEM_COLOR = 0xFFB0B0B0;
    public final static int FAILED_ITEM__COLOR = 0xFFE08090;
    public final static int COMPLETED_ITEM_COLOR = 0xFF50B000;
    public final static int COLLECTION_COLOR = 0xFF808000;
    public final static int DELIVERY_COLOR = 0xFF0070E0;
    public final static int PRICE_COLOR = 0xFFE00000;

    // workaround because Java doesn't allow super() calls with non-static variables (tripItems)
    public static TripItemsAdapter newInstance(Context c, int resource) {
        return new TripItemsAdapter(c, resource, new ArrayList<>());
    }

    private TripItemsAdapter(Context c, int resource, List<TripItem> ti) {
        super(c, resource, ti);
        tripItems = ti;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.trip_item, parent, false);

        // prepare variables
        TextView tv = (TextView) convertView.findViewById(R.id.tvName);
        TripItem ti = getItem(position);
        boolean isSelected = (position == State.trip.currentTripItemIdx);
        TripItem.STATE state = State.trip.tripItems[position].state;

        // choose values
        CharSequence text = TextUtils.concat((position+1) + ". ", tripItemText(ti));
        int typeface = isSelected ? Typeface.BOLD : Typeface.NORMAL;

        // apply
        tv.setText(text);
        tv.setTypeface(null, typeface);
        if (state == TripItem.STATE.NOT_COMPLETED)
            convertView.setBackgroundColor(DEFAULT_ITEM_COLOR);
        else if (state == TripItem.STATE.COMPLETED)
            convertView.setBackgroundColor(COMPLETED_ITEM_COLOR);
        else if (state == TripItem.STATE.MISSED)
            convertView.setBackgroundColor(FAILED_ITEM__COLOR);

        return convertView;
    }

    private CharSequence tripItemText(TripItem ti) {
        String s1 = ti.customerName + " - " + ti.timeFormat.format(ti.visitAt) + ",";
        String s2 = "";
        String noteStr = "";
        SpannableString collectionOrDelivery;
        SpannableString price;
        if (ti.collectionOrDelivery == TripItem.COLLECTION_OR_DELIVERY.COLLECTION)
        {
            collectionOrDelivery = new SpannableString(" collection");
            collectionOrDelivery.setSpan(new ForegroundColorSpan(COLLECTION_COLOR), 0, 11, 0);
        }
        else // on delivery
        {
            collectionOrDelivery = new SpannableString(" delivery");
            collectionOrDelivery.setSpan(new ForegroundColorSpan(DELIVERY_COLOR), 0, 9, 0);

            // inform about bags and hangers
            boolean hasBags = (ti.numberOfBags > 0);
            String bagsString = ti.numberOfBags + " " + "bag(s)";
            if (ti.hasHangers && hasBags)
                s2 += " (" + bagsString + " + hangers)";
            else if (ti.hasHangers && !hasBags)
                s2 += " (only hangers)";
            else if (!ti.hasHangers && hasBags)
                s2 += " (" + bagsString + ")";
            // if (!hasHangers && !hasBags) append nothing
        }

        s2 += ", " + ti.fullAddress;

        String priceStr = "";

        if (ti.collectionOrDelivery == TripItem.COLLECTION_OR_DELIVERY.DELIVERY) // include money owed
            priceStr += String.format(", %s", (Functions.asCurrency(ti.moneyOwed, true)));

        if (ti.state == TripItem.STATE.COMPLETED && ti.paid.compareTo(ti.moneyOwed) != 0) // include paid
            priceStr += String.format(" (%s paid)", Functions.asCurrency(ti.paid, true));

        price = new SpannableString(priceStr);
        price.setSpan(new ForegroundColorSpan(PRICE_COLOR), 0, priceStr.length(), 0);

        if (!ti.notes.isEmpty())
            noteStr = ", " + ti.notes;

        collectionOrDelivery.setSpan(new StyleSpan(Typeface.BOLD), 0, collectionOrDelivery.length(), 0);
        return TextUtils.concat(s1, collectionOrDelivery, s2, price, noteStr);
    }

    public void setTrip(Trip t) {
        TripItem[] tis = t.tripItems;

        tripItems.clear();
        for (TripItem ti : tis)
            tripItems.add(ti);

        notifyDataSetChanged();
    }

    public void clear() {
        tripItems.clear();
        notifyDataSetChanged();
    }

    // On click set item as selected; on another click show dialog
    public static AdapterView.OnItemClickListener getListener() {
        return (arg0, arg1, position, arg3) -> {
            if (State.trip.currentTripItemIdx == position) {
                // show dialog
                TripItemDialog tid = new TripItemDialog();
                Bundle args = new Bundle();
                args.putInt("tripItem", position);
                tid.setArguments(args);
                tid.show(MainActivity.getInstance().getFragmentManager(), "tripItemEdit");
            } else {
                // set as selected
                State.trip.currentTripItemIdx = position;
            }
            MainActivity.refreshList();
        };
    }
}
