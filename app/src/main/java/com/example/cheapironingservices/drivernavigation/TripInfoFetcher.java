package com.example.cheapironingservices.drivernavigation;

import android.util.Log;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TripInfoFetcher {
    public static final String TRIP_URL = "get_daily_trip.json";

    public static String getWebsiteUrl() {
        return State.websiteURL + "/driver_location/";
    }

    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
    }

    private static Request getRequest() {
        HttpUrl url = HttpUrl.parse(getWebsiteUrl() + TRIP_URL)
                .newBuilder()
                .addQueryParameter("username", State.username)
                .addQueryParameter("password", State.password)
                .build();
        return new Request.Builder()
                .url(url)
                .build();
    }

    public static JSONObject blockingRequestTrip() throws JSONException, InvalidCredentialsException, NoTripAssignedException, IOException {
        OkHttpClient client = getClient();
        Request request = getRequest();
        try (Response response = client.newCall(request).execute()) {
            String result = response.body().string();
            if (result.equals("Invalid username or password")) {
                throw new InvalidCredentialsException();
            } else if (result.equals("No trip assigned")) {
                throw new NoTripAssignedException();
            } else if (result.startsWith("Unknown error: ")) {
                result = result.substring("Unknown error: ".length());
                Log.d("get_daily_trip output", result + ", length: " + result.length());
                throw new JSONException("Unhandled server error: " + result);
            } else {
                return new JSONObject(result);
            }
        }
    }
}
