package com.example.cheapironingservices.drivernavigation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import java.math.BigDecimal;

/** Toggle completed and failed for the selected TripItem
 *
 * Activated by clicking on the selected TripItem on the list (i.e. selecting it twice)
 * Has four options: set to [incomplete, complete, failed], and Cancel (go back)
 */
public class TripItemDialog extends DialogFragment {

    private TripItem tripItem = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tripItem = State.trip.tripItems[getArguments().getInt("tripItem")];
    }

    private void onRadioButtonCheck(int radioBtnID, EditText moneyPaidTxt) {
        if (radioBtnID == R.id.itemCompletedRadio) {
            moneyPaidTxt.setEnabled(true);
        } else {
            moneyPaidTxt.setText("0.00");
            moneyPaidTxt.setEnabled(false);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View rootView = (inflater.inflate(R.layout.trip_item_dialog, null));
        final EditText moneyPaidTxt = (EditText) rootView.findViewById(R.id.paidMoneyText);
        final TextView sumOwedHint = (TextView) rootView.findViewById(R.id.sumOwedHint);

        builder.setView(rootView);

        RadioGroup rGroup = (RadioGroup) rootView.findViewById(R.id.statusGroup);
        rGroup.setOnCheckedChangeListener((group, id) -> onRadioButtonCheck(group.getCheckedRadioButtonId(), moneyPaidTxt));

        rootView.findViewById(R.id.dialogConfirmBtn).setOnClickListener(v -> {
            TripItem.STATE newState;
            BigDecimal paid = tripItem.paid;

            if (     ((RadioButton) rootView.findViewById(R.id.itemCompletedRadio)).isChecked())
                newState = TripItem.STATE.COMPLETED;
            else if (((RadioButton) rootView.findViewById(R.id.itemMissedRadio)).isChecked())
                newState = TripItem.STATE.MISSED;
            else
                newState = TripItem.STATE.NOT_COMPLETED;

            String paidStr = moneyPaidTxt.getText().toString();
            if (!paidStr.isEmpty()) {
                // Throw error for inputs like 12.501
                if (paidStr.contains(".") && paidStr.split("\\.")[1].length() >= 3) {
                    Toast.makeText(getActivity(), "Invalid payment amount, too many digits after the decimal point.",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                paid = new BigDecimal(paidStr);
            }
            tripItem.setState(paid, newState);

            MainActivity.refreshList();
            getDialog().cancel();
        });

        rootView.findViewById(R.id.dialogCancelBtn).setOnClickListener(v -> getDialog().cancel());

        sumOwedHint.setText(String.format("(Owed: %s)", Functions.asCurrency(tripItem.moneyOwed, true)));
        int radioBtnID;
        if      (tripItem.state == TripItem.STATE.COMPLETED)
            radioBtnID = R.id.itemCompletedRadio;
        else if (tripItem.state == TripItem.STATE.MISSED)
            radioBtnID = R.id.itemMissedRadio;
        else
            radioBtnID = R.id.itemNotCompletedRadio;
        ((RadioButton) rootView.findViewById(radioBtnID)).setChecked(true);
        onRadioButtonCheck(radioBtnID, moneyPaidTxt);
        moneyPaidTxt.setText(Functions.asCurrency(tripItem.paid, false));

        return builder.create();
    }
}
