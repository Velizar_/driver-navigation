package com.example.cheapironingservices.drivernavigation;

import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import androidx.annotation.Nullable;

public class NextCustomerService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // update state with the change
        boolean customerPaid = intent.getBooleanExtra("customer paid", true);
        State.currentCustomerReached(customerPaid);

        // launch navigation
        try {
            Functions.sendToWaze(this, State.trip.currentTripItem());
        }
        catch (ActivityNotFoundException ex) {
            // return to the main window and report failure
            Intent startActivityIntent = Functions.bringOnTop(this, MainActivity.class);
            startActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startActivityIntent);
            Toast.makeText(this, "Error: Waze not installed.", Toast.LENGTH_SHORT).show();
        }

        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

