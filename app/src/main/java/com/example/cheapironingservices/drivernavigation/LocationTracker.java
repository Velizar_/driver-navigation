package com.example.cheapironingservices.drivernavigation;

import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static androidx.core.content.ContextCompat.checkSelfPermission;

/* Listener class to get coordinates */
public class LocationTracker implements LocationListener {

    private static LocationManager locationManager;
    private static final int SECONDS_BETWEEN_LOCATION_UPDATES = 20;
    private static final int METRES_BETWEEN_LOCATION_UPDATES = 100;

    public LocationTracker(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    private static boolean locationPermission(Activity activity) {
        boolean hasPermission = checkSelfPermission(activity, permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            hasPermission = hasPermission && checkSelfPermission(activity, permission.FOREGROUND_SERVICE) == PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            hasPermission = hasPermission && checkSelfPermission(activity, permission.ACCESS_BACKGROUND_LOCATION) == PERMISSION_GRANTED;
        return hasPermission;
    }

    // request only once, assume it will be granted
    public static void requestPermissionToTrackIfMissing(Activity activity) {
        if (!locationPermission(activity)) {
            List<String> permissions = new ArrayList<>();
            permissions.add(permission.ACCESS_FINE_LOCATION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                permissions.add(permission.FOREGROUND_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                permissions.add(permission.ACCESS_BACKGROUND_LOCATION);
            String[] permArray = new String[permissions.size()];
            for (int i = 0; i < permissions.size(); i++)
                permArray[i] = permissions.get(i);
            ActivityCompat.requestPermissions(
                    activity,
                    permArray,
                    State.PERMISSION_TO_TRACK
            );
        }
    }

    public void tryToStartTracking(Activity activity) {
        if (locationPermission(activity)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    SECONDS_BETWEEN_LOCATION_UPDATES * 1000, METRES_BETWEEN_LOCATION_UPDATES, this);
        }
    }

    public void stop(Activity activity) {
        if (locationPermission(activity))
            locationManager.removeUpdates(this);
    }

    public static final String GEOCODE_TRANSMIT_URL = "receive/";

    public static String getWebsiteUrl() {
        return State.websiteURL + "/driver_location/";
    }

    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
    }

    private static Request getRequest(String latitude, String longitude, String tripItemId, String tripId) {
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", State.username)
                .addFormDataPart("password", State.password)
                .addFormDataPart("lat", latitude)
                .addFormDataPart("long", longitude)
                .addFormDataPart("trip_item_id", tripItemId)
                .addFormDataPart("trip_id", tripId)
                .build();

        return new Request.Builder()
                .url(getWebsiteUrl() + GEOCODE_TRANSMIT_URL)
                .post(requestBody)
                .build();
    }

    public static Callback getGeocodeCallback() {
        return new Callback() {
            @Override public void onFailure(@NotNull Call call, @NotNull IOException e) { }
            @Override public void onResponse(@NotNull Call call, @NotNull Response response) { }
        };
    }

    @Override
    public void onLocationChanged(Location loc) {
        OkHttpClient client = getClient();
        Request request = getRequest(
                Double.toString(loc.getLatitude()),
                Double.toString(loc.getLongitude()),
                State.trip.currentTripItem().id + "",
                State.trip.id + "");
        client.newCall(request).enqueue(getGeocodeCallback());
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

}