package com.example.cheapironingservices.drivernavigation;

import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TripItem implements Serializable {
    public enum COLLECTION_OR_DELIVERY { COLLECTION, DELIVERY }
    public enum STATE {
        NOT_COMPLETED, COMPLETED, MISSED;

        public static STATE toState(String enumString) {
            return valueOf(enumString);
        }
    }

    public final int id;
    public final String lat, lng;
    public final String customerName;
    public final String fullAddress;
    public final BigDecimal moneyOwed;
    public final int numberOfBags;
    public final boolean hasHangers;
    public final String notes;
    public final Date visitAt;

    public STATE state;
    public BigDecimal paid;
    public final COLLECTION_OR_DELIVERY collectionOrDelivery;
    public final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    public static final String PAID_PREF_FORMAT = "TripItem(%s).paid";
    public static final String STATE_PREF_FORMAT = "TripItem(%s).state";

    public TripItem(JSONObject params) throws JSONException {
        String geocodeParts[] = params.getString("geocode").split(",");
        lat = geocodeParts[0];
        lng = geocodeParts[1];
        customerName = params.getString("customerName");
        fullAddress  = params.getString("fullAddress");
        moneyOwed = new BigDecimal(
                       params.getString("moneyOwned"));
        numberOfBags = params.getInt("numberOfBags");
        hasHangers   = params.getBoolean("hasHangers");
        id = params.optInt("id", 0);
        notes = params.optString("notes", "");
        try {
            visitAt = timeFormat.parse(params.getString("visitAt"));
        } catch (ParseException e) {
            // treat it as an input exception
            throw new JSONException("visitAt cannot be parsed: " + params.getString("visitAt"));
        }
        collectionOrDelivery = (moneyOwed.compareTo(new BigDecimal(0)) == 0) ?
                COLLECTION_OR_DELIVERY.COLLECTION : COLLECTION_OR_DELIVERY.DELIVERY;

        paid = new BigDecimal(State.preferences.getString(String.format(PAID_PREF_FORMAT,  id), "0"));
        state = STATE.toState(State.preferences.getString(String.format(STATE_PREF_FORMAT, id), "NOT_COMPLETED"));
    }

    public void setState(BigDecimal newPaid, STATE newVisitState) {
        paid = newPaid;
        state = newVisitState;

        SharedPreferences.Editor editor = State.preferences.edit();
        editor.putString(String.format(PAID_PREF_FORMAT,  id), newPaid.toString());
        editor.putString(String.format(STATE_PREF_FORMAT, id), newVisitState.toString());
        editor.apply();
    }
}
